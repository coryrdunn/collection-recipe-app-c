//
//  ViewController.m
//  CollectionRecipeApp
//
//  Created by Cory Dunn on 10/20/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    arrayOfObjects = [NSMutableArray new];
    
    NSDictionary* d1 = @{@"title": @"Grilled Cheese Sandwich", @"Ingredients":@"Bread, Butter, Cheese", @"Directions":@"Preheat griddle to med-high heat. Spread butter evenly across one side of each slice of bread. Place one slice of bread (butter side down) on griddle. Place cheese on top of bread slice. Add another slice of bread on top (butter side up). Flip sandwich over when bottom slice is golden brown. Remove from heat once both slices of bread are golden brown. Chow down!", @"image":@"http://1.bp.blogspot.com/-hOisAYJTwWE/TrCPTTspz9I/AAAAAAAAM8Y/RIqSBVtAx88/s800/The%2BPerfect%2BGrilled%2BCheese%2BSandwich%2B800%2B1581.jpg"};
    
    NSDictionary* d2 = @{@"title": @"Mac & Cheese", @"Ingredients":@"Box of Kraft Mac & Cheese, Butter, Milk", @"Directions":@"Place a pot of water over high heat until boiling. Add macaroni to water and cook until al-dente. Drain water from pot. Add milk, butter, and powdered cheese. Stir ingredients together and enjoy!", @"image":@"http://www.foodsafetynews.com/files/2015/03/KRAFT_Macaroni-Cheese_Dinner.jpg"};
    
    NSDictionary* d3 = @{@"title": @"Pizza", @"Ingredients":@"Cellphone", @"Directions":@"Pick up cellphone. Call Dominoes. Place order. Make payment. Open door and accept pizza from delivery kid. It's pizza time!", @"image":@"http://www.cicis.com/media/1138/pizza_trad_pepperoni.png"};
    
    
    [arrayOfObjects addObject:d1];
    [arrayOfObjects addObject:d2];
    [arrayOfObjects addObject:d3];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    cv = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:layout];
    cv.delegate = self;
    cv.dataSource = self;
    
    UINib* nib = [UINib nibWithNibName:@"MainCollectionViewCell" bundle:nil];
    [cv registerNib:nib forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:cv];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayOfObjects.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MainCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary* d = arrayOfObjects[indexPath.row];
    
    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:d[@"image"]]];
    cell.imageView.image = [UIImage imageWithData:data];
    cell.lbl.text = d[@"title"];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/2-20, self.view.frame.size.width/2-20);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(60, 15, 10, 15);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"celltouched" sender:indexPath];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"celltouched"]) {
        NSIndexPath* path = (NSIndexPath*)sender;
        DetailViewController* dvc = (DetailViewController*)segue.destinationViewController;
        dvc.details = [arrayOfObjects objectAtIndex:path.row];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
