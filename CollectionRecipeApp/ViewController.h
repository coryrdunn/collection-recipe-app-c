//
//  ViewController.h
//  CollectionRecipeApp
//
//  Created by Cory Dunn on 10/20/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainCollectionViewCell.h"
#import "DetailViewController.h"

@interface ViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

{
    UICollectionView* cv;
    NSMutableArray* arrayOfObjects;
}

@end

