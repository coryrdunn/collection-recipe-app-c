//
//  MainCollectionViewCell.h
//  CollectionRecipeApp
//
//  Created by Cory Dunn on 10/20/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
