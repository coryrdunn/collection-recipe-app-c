//
//  DetailViewController.h
//  CollectionRecipeApp
//
//  Created by Cory Dunn on 10/20/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary* details;
@property (strong, nonatomic) IBOutlet UIImageView *detailImage;
@property (strong, nonatomic) IBOutlet UITextView *ingTxtView;
@property (strong, nonatomic) IBOutlet UITextView *dirTxtView;

@end
